## Key Tasks to Complete

* [ ] Step 1: Create Issues
  * Create an Issue To Track Our Work
    * Navigate to **Plan>Issues** using the lefthand navigation menu
    * Click **New issue**
    * Give a title **Remove Access Token**
    * Click **Assign to me** to assign the issue to yourself. Even if your name dosent appear in the dropdown you can still write in your GitLab id
    * Give a weight (i.e. **1**)
    * Select a due date within two days as this is a high priority issue
    * Leave all other settings as is
    * Click **Create issue**

> [Docs for setting up your org](https://docs.gitlab.com/ee/user/project/quick_actions.html)

* [ ] Step 2: Planning With GitLab Duo Chat
  * What if we didnt know how to create our own issue in GitLab? We can use GitLab Duo Chat to answer these questions and more!
  * In the bottom left corner of your screen go ahead and click the **? Help** link, then click **GitLab Duo Chat**.
  * Lets start by asking chat _"Where can create an issue?"_. You can also ask chat any GitLab related or coding questions. Feel free to use chat as an assistance tool for the rest of the workshop.

* [ ] Step 3: Create a label
  * Labels are an incredibly powerful planning tool in GitLab that we want to take advantage. We will create and "In Progress" and "Vuln Fix" label.
  * Use the left hand navigation menu to click through **Manage** > **Labels**
  * Click **New label**
  * Enter **In Progress** in the ***Title*** field and select a color

* [ ] Step 4: Create New Board
  * Use the lefthand navigation menu to click through **Plan -> Issues boards**
  * Click **New board** next to the dropdown that says **Development**.
  * Enter a title (**Team-Dev-Board**)
  * Leave **Show the Open list** and **Show the Closed list** checkboxes selected
  * Click **Create board**
  * Click **Create list**. Leave the scope of the list set to **Label**. Select **In Progress**. Click **Add to board**.
  * As we add issues in the future if they contain the scopped labels they will automatically appear in the labels board.

* [ ] Step 4: Move Issues in Boards
  * Navigate to **Issues**>**Boards**
  * Click and drag the **Remove Access Token** issue into the **In Progress** list
  * Click on that issue and note the labels now include **In Progress**
  * Click and drag the same issue into Closed
  * Note the issue labels no longer include **In Progress**
  * Before moving on drag the closed issue back to ***In Progress***
